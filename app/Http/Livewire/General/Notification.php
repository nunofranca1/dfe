<?php

namespace App\Http\Livewire\General;

use Livewire\Component;

class Notification extends Component
{
    public function render()
    {
        return view('livewire.general.notification');
    }
}
