<div class="notification">

    <div class="dropdown">
        <a href="#" class="btn btn-default"><i class="far fa-bell"></i></a>
        <div class="dropdown-menu right">
            <div class="heading">
                Notificações
            </div>
            <div class="body scrollbar-macosx">
                <div class="box">

                    <a class="dropdown-item" href="#">
                        <div class="col">
                            <div class="icon blue">
                                <i class="far fa-building"></i>
                            </div>
                        </div>
                        <div class="col">
                            <span class="info">Something else here</span>
                            <small class="time">Há 25 minutos</small>
                        </div>
                    </a>

                </div>
            </div>
            <div class="footer">
                <a href="#" class="btn btn-default btn-block">Ver todas</a>
            </div>
        </div>
    </div>

</div>