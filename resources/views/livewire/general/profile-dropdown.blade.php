<div class="profile">

    <div class="dropdown">
        <a href="#">
            <img
                src="https://www.gravatar.com/avatar/259905cb56f41eff8dcf9d9c0a4cf682?s=200&d=mp&r=x">
        </a>
        <div class="dropdown-menu right">
            <a class="dropdown-item" href="#">Minha conta</a>
            <a class="dropdown-item" href="#">Alterar senha</a>
            <a class="dropdown-item" href="#" wire:click.prevent="logout">Sair</a>
        </div>
    </div>

</div>